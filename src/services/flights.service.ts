import { Database } from '../databases/database_abstract'
import { DatabaseInstanceStrategy } from '../database'
import { Flight, Passenger } from '../databases/mongo/models/flights.model'

export class FlightsService {
    private readonly _db: Database

    constructor() {
        this._db = DatabaseInstanceStrategy.getInstance()
    }

    public async getFlights() {
        return this._db.getFlights()
    }

    public async updateFlightStatus(code: string) {
        return this._db.updateFlightStatus(code)
    }

    public async linkPassenger(code: string, passenger: Passenger) {
        return this._db.linkPassenger(code, passenger)
    }

    public async addFlight(flight: Flight) {
        return this._db.addFlight(flight)
    }
}
