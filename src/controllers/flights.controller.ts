import {
    JsonController,
    Get,
    Param,
    Put,
    Post,
    Body,
} from 'routing-controllers'
import { FlightsService } from '../services/flights.service'
import { Flight, Passenger } from '../databases/mongo/models/flights.model'

@JsonController('/flights', { transformResponse: false })
export default class FlightsController {
    private _flightsService: FlightsService

    constructor() {
        this._flightsService = new FlightsService()
    }

    @Get('')
    async getAll() {
        return {
            status: 200,
            data: await this._flightsService.getFlights(),
        }
    }

    @Put('/:code')
    async updateFlightStatus(@Param('code') code: string) {
        return {
            status: 200,
            data: await this._flightsService.updateFlightStatus(code),
        }
    }

    @Put('/:code/link')
    async linkPassenger(
        @Param('code') code: string,
        @Body() passenger: Passenger
    ) {
        return {
            status: 200,
            data: await this._flightsService.linkPassenger(code, passenger),
        }
    }

    // add flight
    @Post('')
    async addFlight(
        @Body()
        flight: Flight
    ) {
        return {
            status: 200,
            data: await this._flightsService.addFlight(flight),
        }
    }
}
