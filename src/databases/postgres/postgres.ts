import { Database } from '../database_abstract'

import { newDb, IMemoryDb } from 'pg-mem'
import { Flight, Passenger } from '../mongo/models/flights.model'

export class PostgreStrategy implements Database {
    _instance: IMemoryDb
    static _instance: IMemoryDb

    constructor() {
        this.getInstance()
    }
    updateFlightStatus(code: string): Promise<Flight> {
        throw new Error('Method not implemented.')
    }
    linkPassenger(code: string, passenger: Passenger): Promise<Flight> {
        throw new Error('Method not implemented.')
    }

    async getInstance() {
        const db = newDb()

        db.public.many(`
            CREATE TABLE flights (
                code VARCHAR(5) PRIMARY KEY,
                origin VARCHAR(50),
                destination VARCHAR(50),
                status VARCHAR(50)
            );
        `)

        db.public.many(`
            INSERT INTO flights (code, origin, destination, status)
            VALUES ('LH123', 'Frankfurt', 'New York', 'on time'),
                     ('LH124', 'Frankfurt', 'New York', 'delayed'),
                        ('LH125', 'Frankfurt', 'New York', 'on time')
        `)

        PostgreStrategy._instance = db

        return db
    }

    public async getFlights() {
        return PostgreStrategy._instance.public.many('SELECT * FROM flights')
    }

    public async addFlight(flight: Flight) {
        return PostgreStrategy._instance.public.many(
            `INSERT INTO flights (code, origin, destination, status) VALUES ('${flight.code}', '${flight.origin}', '${flight.destination}', '${flight.status}')`
        ) as unknown as Flight
    }
}
