import { MaxLength } from 'class-validator'
import mongoose, { Schema } from 'mongoose'

export class Flight {
    @MaxLength(5)
    code: string
    origin: string
    destination: string
    status: string
    passengers: Passenger[]
}

export class Passenger {
    name: string
    gender: string
    email: string
}

const schema = new Schema<Flight>(
    {
        code: { required: true, type: String },
        origin: { required: true, type: String },
        destination: { required: true, type: String },
        status: String,
        passengers: { required: false, type: [] },
    },
    { timestamps: true }
)

export const FlightsModel = mongoose.model('Flights', schema)
