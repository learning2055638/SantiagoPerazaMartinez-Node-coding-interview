import { Database } from '../database_abstract'
import mongoose, { Document } from 'mongoose'
import { MongoMemoryServer } from 'mongodb-memory-server'

import { Flight, FlightsModel, Passenger } from './models/flights.model'
import { loadData } from '../../../scripts/base-data'

export class MongoStrategy implements Database {
    constructor() {
        this.getInstance()
    }
    _instance: any
    updateFlightStatus(code: string): Promise<Flight> {
        throw new Error('Method not implemented.')
    }
    async linkPassenger(code: string, passenger: Passenger): Promise<Flight> {
        const flight = await FlightsModel.findOne({
            code,
        })

        if (!flight.passengers) {
            flight.passengers = []
        }
        flight.passengers.push(passenger)
        await flight.save()
        return flight as Flight
    }

    addFlight(flight: Flight): Promise<Flight> {
        return FlightsModel.create(flight)
    }

    public async getFlights() {
        return FlightsModel.find({})
    }

    async getInstance() {
        const mongo = await MongoMemoryServer.create()
        const uri = mongo.getUri()

        const mongooseOpts = {
            useNewUrlParser: true,
            useCreateIndex: true,
            useUnifiedTopology: true,
            useFindAndModify: false,
        }

        try {
            await mongoose.connect(uri, mongooseOpts)
            await loadData()
        } catch (error) {
            console.error('Failed to connect or load data', error)
        }
    }
}
