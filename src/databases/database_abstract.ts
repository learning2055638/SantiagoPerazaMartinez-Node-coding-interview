import { Flight, Passenger } from './mongo/models/flights.model'

export interface Database {
    getInstance(): any

    getFlights(): Promise<Flight[]>

    updateFlightStatus(code: string): Promise<Flight>

    linkPassenger(code: string, passenger: Passenger): Promise<Flight>

    addFlight(flight: Flight): Promise<Flight>
}
